#!/usr/bin/env python3

import django
import argparse
import json
import sys

django.setup()

from CIResults.models import RunConfig  # noqa
from CIResults.rest_views import RunConfigViewSet  # noqa


def get_runconfig_or_die(name):
    try:
        return RunConfig.objects.get(name=name)
    except RunConfig.DoesNotExist:
        print("ERROR: The runconfig '{}' does not exist in the database".format(name))
        sys.exit(1)


# Parse the options
parser = argparse.ArgumentParser()
parser.add_argument("runcfg", help="Name of the runconfig you want the known issues for")
parser.add_argument("-i", "--indent", type=int, choices=range(0, 8),
                    help="Amount of indentation you want for your JSON", default=None)

args = parser.parse_args()

runcfg = get_runconfig_or_die(name=args.runcfg)
print(json.dumps(RunConfigViewSet.known_failures_serialized(runcfg).data, indent=args.indent))

sys.exit(0)
