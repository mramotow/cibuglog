# Generated by Django 2.1.5 on 2019-04-01 18:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('CIResults', '0040_bugtracker_project'),
    ]

    operations = [
        migrations.AddField(
            model_name='bug',
            name='description',
            field=models.TextField(blank=True, help_text='Description of the bug report. To be filled automatically', null=True),
        ),
        migrations.AlterField(
            model_name='bugtracker',
            name='url',
            field=models.URLField(help_text='Public URL to the bugtracker (e.g. gitlab: https://gitlab.freedesktop.org bugzilla: https://bugs.freedesktop.org)'),
        ),
    ]
